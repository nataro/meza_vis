from application.api.models import SensorData

from django.views import generic

# class ChartsByItemView(LoginRequiredMixin, generic.TemplateView):
class ChartsByItemView(generic.TemplateView):
    template_name = "charts/charts_by_item.html"

    # chart用のデータはhtmlから直接取得手続きを取る(RESTapi経由で取得)からcontextに渡す必要はない
    # テンプレートに自分が定義したデータを渡したい
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)  # はじめに継承元のメソッドを呼び出す
        context["template_iteration"] = range(3)  # テンプレートでforループするときに使う
        return context
