from django.conf.urls import url
from . import views

app_name = 'charts'
urlpatterns = [
    # url(r'^sensor-data-table/', views.SensorDataListPeginate.as_view(), name='sensor_table'),
    url(r'^sensor-data-chart/by-item/', views.ChartsByItemView.as_view(), name='sensor_chart_by_item'),
    # url(r'^sensor-data-chart/by-sensor/', views.ChartsBySensorView.as_view(), name='sensor_chart_by_sensor'),
]