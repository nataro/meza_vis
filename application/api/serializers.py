from rest_framework import serializers
from .models import SensorData


class SensorDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorData
        fields = (
        'time', 'json_01', 'json_02', 'json_03', 'json_04', 'json_05', 'json_06', 'json_07', 'json_08', 'json_09',
        'json_10', 'json_11', 'json_12', 'json_13', 'json_14', 'json_15', 'json_16', 'json_17', 'json_18',
        'json_19', 'json_20', 'json_21', 'json_22', 'json_23', 'json_24', 'json_25', 'json_26', 'json_27',
        'json_28', 'json_29', 'json_30')
