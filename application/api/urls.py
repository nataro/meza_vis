from django.conf.urls import url
from . import views

app_name = 'api'
urlpatterns = [
    # センサデータのリスト
    url(r'^list/$',
        views.SensorDataList.as_view(),
        name='data_list'),
    # 「****/**/**」という日付限定の正規表現を指定(GET想定)
    url(r'^chart/(?P<u_year>\d{2,4})/(?P<u_month>\d{1,2})/(?P<u_day>\d{1,2})/$',
        views.ChartDataDate.as_view(),
        name='data_for_chart'),
    # 外部（ゲートウェイ等）からのデータをPOSTしてデータベースに登録するためのもの
    url(r'^create/$',
        views.SensorDataCreate.as_view(),
        name='data_create'),
]