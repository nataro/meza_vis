from .models import SensorData
from .serializers import SensorDataSerializer

from django.http import JsonResponse

from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework import status
from rest_framework import generics, mixins

import time


class SensorDataCreate(generics.CreateAPIView):
    '''
    generic view で外部からセンサーノードのデータをPOSTで受ける専用の関数を作ってみる
    '''
    # 外部からJWTでGET、POSTのみ許可
    # authentication_classes = (JSONWebTokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    serializer_class = SensorDataSerializer


class SensorDataList(generics.ListAPIView):
    '''
    リスト表示用のAPI
    settings.pyでペジネーションを設定した場合、その設定内容が反映される
    '''
    # authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication, BasicAuthentication)
    # permission_classes = (IsAuthenticated,)
    serializer_class = SensorDataSerializer

    def get_queryset(self):
        return SensorData.objects.all()


class ChartDataDate(APIView):
    # テンプレートからcsrfなどを付けて呼び出される想定
    # authentication_classes = (SessionAuthentication, BasicAuthentication)
    # permission_classes = (IsAuthenticated,)


    def get(self, request, u_year, u_month, u_day, format=None):
        # specified_by_model = SensorData.objects.all().filter(model=sensor_model)
        orderd_by_time = SensorData.objects.all().filter(time__year=u_year,
                                                         time__month=u_month,
                                                         time__day=u_day).order_by('time')

        sensor_data = queryset_2_highchart_datetime_vs_time_humid_atmo(orderd_by_time)

        # JSONでレスポンスを返すなら、手っ取り早いJsonResponseが楽
        return JsonResponse(sensor_data)


def queryset_2_highchart_datetime_vs_time_humid_atmo(django_queryset):
    '''
    :param django_queryset:
    :return:
    highchartのdatetimeはmilisedondsで渡すと素直に表示されるみたい。
    SendorData.cbjectsから受け取った直後は、datetime.datetime()型なので、milisecondsの値となるように以下の処理を行う
    さらに、REST-frameWorkでdatatime型を返すとき、自動的にタイムゾーン分の時間がオフセットされてかえされるみたい。
    この問題の対策については、HighChartに渡すデータにのみ、'Asia/Tokyo'の時間分(9時間)を加算して対応することにする。
    なぜか、9時間ではなくて18時間加算すると上手くいく

    '''

    # djangoのQuerySetから'time'という名称のカラムのデータを受け取る
    # このとき flat=True にしないとタプルになったデータが返ってくる
    times = django_queryset.values_list('time', flat=True)
    # milisecondsに換算。なぜか、9時間ではなくて18時間加算すると上手くいく。
    tz_offset = 18 * 3600 * 1000
    times = [time.mktime(x.timetuple()) * 1000 + tz_offset for x in times]
    # このときtimesの中身は
    # [ 1515683544000.0, … , 1515683544000.0 ] こんな感じになっている

    # "json_01","json_02",…,"json_30"というJSON形式のデータが格納してあるという前提でそのカラム名のリストを作成する
    calam_name_list = ["json_{0:02d}".format(n) for n in range(1, 31)]

    # return するデータの格納先
    sensor_data = {}

    for calam_name in calam_name_list:
        # 先に作成しておいたカラム名のリストに相当するデータを受け取っていく
        data_list = django_queryset.values_list(calam_name, flat=True)
        # data_list の中身は
        # [ {"atmo": 1213, "temp": 15.5, "humid": 16.2}, … , {"atmo": 1113, "temp": 17.5, "humid": 15.2} ]

        # Highchart に渡すデータは、温度などの要素ごとに[[time1,temp1],[time2,temp2],・・・]という形にしておきたい
        # collections の defaultdict を用いて成形したデータを作成する
        from collections import defaultdict
        listed_dict = defaultdict(list)

        for (t, dic) in zip(times, data_list):
            if (dic):
                atmo = dic.get('atmo')
                temp = dic.get('temp')
                humid = dic.get('humid')
                listed_dict['atmos'].append([t, atmo])
                listed_dict['temps'].append([t, temp])
                listed_dict['humids'].append([t, humid])

        # 成形された listed_dict は ↓こんな感じ
        # { 'atmo' : [ [1515683544000.0, 1213], … , [1515683544000.0, 1113] ],
        #   'temp' : [ [1515683544000.0, 15.5], … , [1515683544000.0, 1113] ],
        #   'humid' : [ [1515683544000.0, 16.2], … , [1515683544000.0, 15.2] ] }

        sensor_data[calam_name] = listed_dict

    return sensor_data
